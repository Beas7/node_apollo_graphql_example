const { gql } = require("apollo-server-express");
const typeDefs = gql`
    type User {
        id: Int!,
        name: String!,
        age: Int!,
        married: Boolean!
    }

    # Queries
    type Query {
        getAllUsers: [User!]!
        getUser(id: Int!): User!
    }

    # Mutations
    type Mutation {
        createUser(id: Int, name: String!, age: Int!, married: Boolean!): User!
        updateUser(id: Int!, name: String, age: Int, married: Boolean): User!
        deleteUser(id: Int!): User!
    }
`;

module.exports = { typeDefs };