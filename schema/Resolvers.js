const { UserInputError } = require('apollo-server-express');
const { users } = require('../dummyData');
const resolvers = {
    Query: {
        getAllUsers() {
            return users;
        },
        getUser(parent, args) {
            const user = users.find(u => u.id === parseInt(args.id));
            //if (!user) throw new UserInputError(`The user ${args.id} was not found`);
            return user;
        }
    },
    Mutation: {
        createUser(parent, args) {
            const newUser = args;
            newUser.id = users.length;
            users.push(newUser);
            return newUser;
        },
        updateUser(parent, args) {
            const user = users.find(u => u.id === parseInt(args.id));
            const index = users.indexOf(user);
            if (index < 0) throw new UserInputError(`The user ${args.id} was not found`);
            args.name ? users[index].name = args.name : console.log("The user property was not found - name");
            args.age ? users[index].age = args.age : console.log("The user property was not found - age");
            args.married !== undefined ? users[index].married = args.married : console.log("The user property was not found - married");

            return user;
        },
        deleteUser(parent, args) {
            const user = users.find(u => u.id === parseInt(args.id));
            const index = users.indexOf(user);
            console.log(index);
            if (index < 0) throw new UserInputError(`The user ${args.id} was not found`);

            users.splice(index, 1);
            return user;
        }
    }
};

module.exports = { resolvers }