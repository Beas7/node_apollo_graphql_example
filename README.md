# Node_Apollo_GraphQL_example

Using Apollo Server Express

GraphQL is a middleware that will run before every request and it has been pre-made for us to deal with data. <br><br>

**Usage:**
<pre>
npm install
nodemon index.js
In your browser: http://localhost:3001/graphql
</pre>

**Get query example**
<pre>
query ExampleQuery {
  getAllUsers {
    id
    name
    age
    married
  }
}

query {
  getUser(id: 1) {
    id
    name
    age
    married
  }
}
</pre>


**Create Mutation query examples:**
<pre>
mutation {
    createUser(name: "John", age: 44, married: false) {
        id,
        name,
        age,
        married
    }
}

mutation {
    updateUser(id: 1, name: "Joel", age: 29, married: false) {
      id
      name
      age
      married
  }
}

mutation {
    deleteUser(id: 3) {
      id
      name
      age
      married
  }
}
</pre>