const { ApolloServer } = require("apollo-server-express");
const { typeDefs } = require('./schema/TypeDefs');
const { resolvers } = require('./schema/Resolvers');
const express = require("express");
const app = express();


const server = new ApolloServer({ typeDefs, resolvers });

(async () => {
    
    await server.start() // to avoid start-up errors
    
    server.applyMiddleware({ app });

})();



const port = process.env.PORT || 3001;
app.listen(port, () => {
    console.log(`SERVER RUNNING ON PORT ${port}...`);
});