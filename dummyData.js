let users = [
    {
        id: 1,
        name: "Joel",
        age: 30,
        married: true
    },
    {
        id: 2,
        name: "Claudia",
        age: 25,
        married: true
    },
    {
        id: 3,
        name: "Pedro",
        age: 21,
        married: false
    },
    {
        id: 4,
        name: "Jessica",
        age: 26,
        married: false
    },
    {
        id: 5,
        name: "Ismael",
        age: 36,
        married: false
    },
    {
        id: 6,
        name: "Laura",
        age: 35,
        married: true
    },
    {
        id: 7,
        name: "Angela",
        age: 19,
        married: false
    },
];

module.exports = { users };